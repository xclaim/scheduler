package co.id.bni.scheduler.model.dto;

public class PdfDto {

    private String name;
    private String cardNumber;
    private String accountNumber;
    private String cardPin;
    private String strPw;

    public PdfDto(String name, String cardNumber, String accountNumber, String cardPin, String strPw) {
        this.name = name;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.cardPin = cardPin;
        this.strPw = strPw;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCardPin() {
        return cardPin;
    }

    public void setCardPin(String cardPin) {
        this.cardPin = cardPin;
    }

    public String getStrPw() {
        return strPw;
    }

    public void setStrPw(String strPw) {
        this.strPw = strPw;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
