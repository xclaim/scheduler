package co.id.bni.scheduler.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Receipt {
    private String custName;
    private String refNum;
    private String registerDate;
    private String registerTime;
    private String newAccountNum;
    private String accountType;
    private String cardNum;
    private String accountBranch;
    private String branchName;
    private String accStatus;
    private String initialDeposit;
    private String lastDepositDate;
    @JsonIgnore
    private String email;
    @JsonIgnore
    private String cif;
    @JsonIgnore
    private String tglLahir;
    @JsonIgnore
    private String cardPin;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getRefNum() {
        return refNum;
    }

    public void setRefNum(String refNum) {
        this.refNum = refNum;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public String getNewAccountNum() {
        return newAccountNum;
    }

    public void setNewAccountNum(String newAccountNum) {
        this.newAccountNum = newAccountNum;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getAccountBranch() {
        return accountBranch;
    }

    public void setAccountBranch(String accountBranch) {
        this.accountBranch = accountBranch;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getInitialDeposit() {
        return initialDeposit;
    }

    public void setInitialDeposit(String initialDeposit) {
        this.initialDeposit = initialDeposit;
    }

    public String getLastDepositDate() {
        return lastDepositDate;
    }

    public void setLastDepositDate(String lastDepositDate) {
        this.lastDepositDate = lastDepositDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getCardPin() {
        return cardPin;
    }

    public void setCardPin(String cardPin) {
        this.cardPin = cardPin;
    }
}
