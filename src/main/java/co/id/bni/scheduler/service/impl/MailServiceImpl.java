package co.id.bni.scheduler.service.impl;

import co.id.bni.scheduler.constant.WebGuiConstant;
import co.id.bni.scheduler.model.Receipt;
import co.id.bni.scheduler.model.dto.MailDto;
import co.id.bni.scheduler.model.dto.PdfDto;
import co.id.bni.scheduler.service.MailService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender mailSender;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    @Value("${attachment.path}")
    private String attachPath;

    @Value("${link.host}")
    private String linkHost;

    @Value("${link.ref.path}")
    private String linkRefPath;

    @Override
    public void sendMail(MailDto mailDto, Receipt receipt,File pdfAttachment) {
        String body = "<html><head></head><body>\n" +
                "    <p>&nbsp;</p>\n" +
                "    <p>Dear "+receipt.getCustName()+",</p>\n" +
                "    <p>Congratulations, Your BNI Account is now Open via BNI Digital Account</p>\n" +
                "    <p>&nbsp;</p>\n" +
                "    <p>Please make an initial deposit immediately at least IDR "+receipt.getInitialDeposit()+" Before "+receipt.getLastDepositDate()+" or your account will automatically closed</p>\n" +
                "    <p>Don't forget to activate <b>BNI Mobile Banking</b> for your everyday transaction. For more info visit <a href=\"http://bit.ly/RegisBNIMobileBanking\">bit.ly/RegisBNIMobileBanking</a></p>\n" +
                "\t<p>&ldquo;You can get your physical debit card via <b>BNI DigiCS</b> or <b>nearest BNI Branch Office</b>. You can pick up your saving books at nearest BNI Branch Office during operational hours. Don't forget to prepare your KTP/ID Card and account opening proof .&rdquo;</p>\n" +
                "\t<p>This email attachment contains your account number, card number and card pin, open it with your birthday (ddMMyyyy) as password </p>\n" +
                "\t<p>Note : This is automated message, please don't reply this email. For more information Call BNI Call at 1500046.</p>\n" +
                "    <p>&nbsp;</p>\n" +
                "    <p>Best&nbsp;regards,</p>\n" +
                "    <p>PT Bank Negara Indonesia (Persero) Tbk.</p>\n" +
                "</body></html>";
        try{
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper messageHelper = setMailProp(message, mailDto, body,true);
            FileSystemResource attachFile = new FileSystemResource(pdfAttachment);
            messageHelper.addAttachment(pdfAttachment.getName(),attachFile);
            mailSender.send(message);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void sendMail(MailDto mailDto, Receipt receipt) {
        String refUrl = WebGuiConstant.HTTP+linkHost+linkRefPath;
        String body = "<html><head></head><body>\n" +
                "    <p>&nbsp;</p>\n" +
                "    <p>Dear "+receipt.getCustName()+",</p>\n" +
                "    <p>Congratulations, Your BNI Account is now Open via BNI Digital Account</p>\n" +
                "    <p>&nbsp;</p>\n" +
                "    <p>Please make an initial deposit immediately at least IDR "+receipt.getInitialDeposit()+" Before "+receipt.getLastDepositDate()+" or your account will automatically closed</p>\n" +
                "    <p>Don't forget to activate <b>BNI Mobile Banking</b> for your everyday transaction. For more info visit <a href=\"http://bit.ly/RegisBNIMobileBanking\">bit.ly/RegisBNIMobileBanking</a></p>\n" +
                "\t<p>&ldquo;You can get your physical debit card via <b>BNI DigiCS</b> or <b>nearest BNI Branch Office</b>. You can pick up your saving books at nearest BNI Branch Office during operational hours. Don't forget to prepare your KTP/ID Card and account opening proof .&rdquo;</p>\n" +
                "\t<p>Check your account status by clicking link bellow and enter "+receipt.getRefNum()+" as your reference number </p>\n" +
                "\t<p><a href=\""+refUrl+"\">"+refUrl+"</a></p>\n"+
                "\t<p>Note : This is automated message, please don't reply this email. For more information Call BNI Call at 1500046.</p>\n" +
                "    <p>&nbsp;</p>\n" +
                "    <p>Best&nbsp;regards,</p>\n" +
                "    <p>PT Bank Negara Indonesia (Persero) Tbk.</p>\n" +
                "</body></html>";
        try{
            MimeMessage message = mailSender.createMimeMessage();
            setMailProp(message, mailDto, body, false);
            mailSender.send(message);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private MimeMessageHelper setMailProp(MimeMessage message, MailDto mailDto, String body, boolean multipart) throws MessagingException {
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, multipart, "UTF-8");
        messageHelper.setTo(mailDto.getTo().trim());
        messageHelper.setFrom(mailDto.getFrom().trim());
        messageHelper.setSubject(mailDto.getSubject());
        messageHelper.setText(body,true);
        return messageHelper;
    }

    @Override
    public String createPdfAttachment(PdfDto pdfDto) {
        File file = new File(attachPath);
        try{
            if(!file.exists()){
                boolean isDirCreated = file.mkdir();
                if(!isDirCreated){
                    return "";
                }
            }
            PDDocument document = new PDDocument();
            PDPage page = new PDPage();
            document.addPage(page);
            AccessPermission accessPermission = new AccessPermission();
            accessPermission.setCanPrint(true);
            accessPermission.setCanModify(false);
            StandardProtectionPolicy standardProtectionPolicy = new StandardProtectionPolicy(pdfDto.getStrPw()+WebGuiConstant.USER_PW,pdfDto.getStrPw(),accessPermission);
            document.protect(standardProtectionPolicy);
            PDPageContentStream content = new PDPageContentStream(document,page);
            content.setFont(PDType1Font.TIMES_ROMAN,12);
            content.beginText();
            //Setting the leading
            content.setLeading(14.5f);
            //Setting the position for the line
            content.newLineAtOffset(25, 725);
            content.showText(WebGuiConstant.OPEN_MESSAGE);
            content.newLine();
            content.newLine();
            content.showText(WebGuiConstant.CARD_NUM+pdfDto.getCardNumber());
            content.newLine();
            content.showText(WebGuiConstant.ACC_NUM+pdfDto.getAccountNumber());
            content.newLine();
            content.showText(WebGuiConstant.CARD_PIN+pdfDto.getCardPin());
            content.newLine();
            content.newLine();
            content.showText(WebGuiConstant.DISCLAIMER);
            content.newLine();
            content.showText(WebGuiConstant.DISCLAIMER2);
            content.newLine();
            content.showText(WebGuiConstant.REMINDER);
            content.newLine();
            content.newLine();
            content.showText(WebGuiConstant.THANK_YOU);
            content.endText();
            content.close();
            String filename = attachPath+"/"+sdf.format(new Date())+"-"+pdfDto.getName()+".pdf";
            document.save(filename);
            document.close();
            return filename;
        }catch(Exception e){
            e.printStackTrace();
            return "";
        }

    }
}
