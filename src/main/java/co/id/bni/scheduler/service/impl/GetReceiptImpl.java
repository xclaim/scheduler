package co.id.bni.scheduler.service.impl;

import co.id.bni.scheduler.model.Receipt;
import co.id.bni.scheduler.service.GetReceipt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class GetReceiptImpl implements GetReceipt {

    private final Logger log = LoggerFactory.getLogger(GetReceiptImpl.class);

    @Autowired
    private DataSource dataSource;

    private final String query = "select concat(cln.nama_depan,' ',\n" +
            "case when cln.nama_tengah is not null then concat(cln.nama_tengah,' ') else '' end,\n" +
            "cln.nama_belakang) as full_name,\n" +
            "cln.reference_number  as reff_num,\n" +
            "date_format(cln.created_date, '%d %M %Y') as register_date, \n" +
            "concat(date_format(cln.created_date, '%H:%i:%s'),' WIB') as register_time, \n" +
            "i.nomor_rekening as new_account_num,\n" +
            "st.name as account_type,\n" +
            "i.nomor_kartu  as card_num,\n" +
            "'KCP MABES TNI CILANGKAP' as account_branch,\n" +
            "'KCP MABES TNI CILANGKAP' as branch_name,\n" +
            "'Gagal Membuat Pin' as acc_status,\n" +
            "'250.000,-' as initial_deposit,\n" +
            "'25 Desember 2021' as last_deposit_date,\n" +
            "cln.alamat_email as alamat_email,\n" +
            "i.cif as cif,\n" +
            "date_format(str_to_date(cln.tanggal_lahir,'%Y-%m-%d'),'%d%m%Y') as tanggal_lahir,\n" +
            "i.card_pin as card_pin\n" +
            "from customerln cln \n" +
            "join inventory i on cln.no_identitas = i.no_identitas \n" +
            "join saving_type st on cln.jenis_tabungan = st.code\n" +
            "where i.status_send_email = '0'";

    private final String updateInventorySql = "update inventory i set i.status_send_email = '1' where i.cif  = ?";

    @Override
    public List<Receipt> getReceipt() {
        List<Receipt> receipts = new ArrayList<Receipt>();
        try{
            Connection con = dataSource.getConnection();
            PreparedStatement preparedStatement = con.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Receipt receipt = new Receipt();
                receipt.setCustName(resultSet.getString("full_name"));
                receipt.setRefNum(resultSet.getString("reff_num"));
                receipt.setRegisterDate(resultSet.getString("register_date"));
                receipt.setRegisterTime(resultSet.getString("register_time"));
                receipt.setNewAccountNum(resultSet.getString("new_account_num"));
                receipt.setAccountType(resultSet.getString("account_type"));
                receipt.setCardNum(resultSet.getString("card_num"));
                receipt.setAccountBranch(resultSet.getString("account_branch"));
                receipt.setBranchName(resultSet.getString("branch_name"));
                receipt.setAccStatus(resultSet.getString("acc_status"));
                receipt.setInitialDeposit(resultSet.getString("initial_deposit"));
                receipt.setLastDepositDate(resultSet.getString("last_deposit_date"));
                receipt.setEmail(resultSet.getString("alamat_email"));
                receipt.setCif(resultSet.getString("cif"));
                receipt.setTglLahir(resultSet.getString("tanggal_lahir"));
                receipt.setCardPin(resultSet.getString("card_pin"));
                receipts.add(receipt);
            }
            con.close();
        }catch(Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return receipts;
    }

    @Override
    public boolean updateInventory(String cif) {
        try{
            Connection con = dataSource.getConnection();
            PreparedStatement prep = con.prepareStatement(updateInventorySql);
            prep.setString(1,cif);
            prep.executeUpdate();
            con.close();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
            return false;
        }
    }
}
