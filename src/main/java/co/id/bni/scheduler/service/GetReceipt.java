package co.id.bni.scheduler.service;

import co.id.bni.scheduler.model.Receipt;

import java.util.List;

public interface GetReceipt {

    List<Receipt> getReceipt();
    boolean updateInventory(String cif);
}
