package co.id.bni.scheduler.service;

import co.id.bni.scheduler.model.Receipt;
import co.id.bni.scheduler.model.dto.MailDto;
import co.id.bni.scheduler.model.dto.PdfDto;

import java.io.File;

public interface MailService {

    void sendMail(MailDto mailDto, Receipt receipt, File pdfAttachment);

    void sendMail(MailDto mailDto, Receipt receipt);

    String createPdfAttachment(PdfDto pdfDto);
}
