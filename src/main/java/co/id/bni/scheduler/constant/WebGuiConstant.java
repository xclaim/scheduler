package co.id.bni.scheduler.constant;

public final class WebGuiConstant {

    public static final String HTTP = "http://";
    public static final String HTTPS = "https://";

    public static final String ATTACH_PATH = "/attachment";
    public static final String OPEN_MESSAGE = "Congratulations, Your BNI Account is now open. Here are your account number, card number, and your card pin";
    public static final String CARD_NUM = "Card Number : ";
    public static final String ACC_NUM = "Account Number : ";
    public static final String CARD_PIN = "Card Pin : ";
    public static final String DISCLAIMER = "Please change your default card pin immediately at nearest BNI branch office, " +
            "and don't forget to make an initial ";
    public static final String DISCLAIMER2 = "deposit or your account will automatically closed";
    public static final String REMINDER = "You can also get your debit card and your saving book at nearest BNI branch office";
    public static final String THANK_YOU = "Thank you";

    public static final String USER_PW = "UnL0Ck7h3pdfF1l3";
    public static final String MAIL_SUBJECT = "Pembukaan Rekening BNI";
}
