package co.id.bni.scheduler.task;

import co.id.bni.scheduler.constant.WebGuiConstant;
import co.id.bni.scheduler.model.Receipt;
import co.id.bni.scheduler.model.dto.MailDto;
import co.id.bni.scheduler.model.dto.PdfDto;
import co.id.bni.scheduler.service.GetReceipt;
import co.id.bni.scheduler.service.MailService;
import co.id.bni.scheduler.util.DummyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component("scheduledMessage")
public class ScheduledMessage {

    @Autowired
    private GetReceipt getReceipt;

    @Autowired
    private MailService mailService;

    @Value("${mail.server}")
    private String mailServerAddress;

    private final Logger log = LoggerFactory.getLogger(ScheduledMessage.class);

    @Scheduled(cron = "${cron.expression}")
    public void sendMail(){
        try{
            //List<Receipt> receiptList = DummyUtil.getDummyReceipt();
            List<Receipt> receiptList = getReceipt.getReceipt();
            if(!receiptList.isEmpty()){
                for(Receipt receipt: receiptList){
                    PdfDto pdfDto = new PdfDto(receipt.getCustName(),receipt.getCardNum(),receipt.getNewAccountNum(),receipt.getCardPin(),receipt.getTglLahir());
                    String attachmentPath = mailService.createPdfAttachment(pdfDto);
                    if(!attachmentPath.isEmpty()){
                        File file = new File(attachmentPath);
                        if(file.exists()){
                            log.info("generating "+file.getAbsolutePath()+" success");
                            MailDto mailDto = new MailDto(receipt.getEmail(),mailServerAddress, WebGuiConstant.MAIL_SUBJECT);
                            log.info("send mail to "+mailDto.getTo()+" with no acc "+receipt.getNewAccountNum());
                            mailService.sendMail(mailDto,receipt,file);
                            log.info("Mail send check inbox");
                            boolean inventoryUpdated = getReceipt.updateInventory(receipt.getCif());
                            if(inventoryUpdated){
                                log.info("update inventory with cif "+receipt.getCif()+" success");
                            }else {
                                log.error("update inventory with cif "+receipt.getCif()+" failed");
                            }
                        }else{
                            log.error("Failed to generate "+attachmentPath);
                        }
                    }else {
                        log.error("Failed to generate "+attachmentPath);
                    }
                }
            }else {
                log.error("Data not found");
            }

        }catch(Exception e){
            e.printStackTrace();
        }

    }

}
