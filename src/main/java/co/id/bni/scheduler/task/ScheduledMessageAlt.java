package co.id.bni.scheduler.task;

import co.id.bni.scheduler.constant.WebGuiConstant;
import co.id.bni.scheduler.model.Receipt;
import co.id.bni.scheduler.model.dto.MailDto;
import co.id.bni.scheduler.service.GetReceipt;
import co.id.bni.scheduler.service.MailService;
import co.id.bni.scheduler.util.DummyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("scheduledMessageAlt")
public class ScheduledMessageAlt {

    @Autowired
    private GetReceipt getReceipt;

    @Autowired
    private MailService mailService;

    @Value("${mail.server}")
    private String mailServerAddress;

    private final Logger log = LoggerFactory.getLogger(ScheduledMessageAlt.class);

    @Scheduled(cron = "${cron.expression.alt}")
    public void sendMailAlt(){
        try{
            //List<Receipt> receiptList = DummyUtil.getDummyReceipt();
            List<Receipt> receiptList = getReceipt.getReceipt();
            if(!receiptList.isEmpty()){
                for(Receipt receipt : receiptList){
                    MailDto mailDto = new MailDto(receipt.getEmail(),mailServerAddress, WebGuiConstant.MAIL_SUBJECT);
                    log.info("send mail to "+mailDto.getTo()+" with no acc "+receipt.getNewAccountNum());
                    mailService.sendMail(mailDto,receipt);
                    log.info("Mail send check inbox");
                    boolean inventoryUpdated = getReceipt.updateInventory(receipt.getCif());
                    if(inventoryUpdated){
                        log.info("update inventory with cif "+receipt.getCif()+" success");
                    }else {
                        log.error("update inventory with cif "+receipt.getCif()+" failed");
                    }
                }
            }else {
                log.error("Data not found");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
