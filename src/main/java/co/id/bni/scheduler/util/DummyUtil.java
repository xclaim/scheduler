package co.id.bni.scheduler.util;

import co.id.bni.scheduler.model.Receipt;

import java.util.ArrayList;
import java.util.List;

public class DummyUtil {

    public static List<Receipt> getDummyReceipt(){
        List<Receipt> receiptList = new ArrayList<>();
        Receipt receipt = new Receipt();
        receipt.setCustName("IIN KAPRIATI");
        receipt.setRefNum("20211125140852525737");
        receipt.setRegisterDate("25 November 2021");
        receipt.setRegisterTime("14:08:52 WIB");
        receipt.setNewAccountNum("1000115378");
        receipt.setAccountType("BNI Taplus");
        receipt.setCardNum("5264229990009669");
        receipt.setAccountBranch("KCP MABES TNI CILANGKAP");
        receipt.setBranchName("KCP MABES TNI CILANGKAP");
        receipt.setAccStatus("Gagal Membuat Pin");
        receipt.setInitialDeposit("250.000,-");
        receipt.setLastDepositDate("25 Desember 2021");
        receipt.setEmail("kusnendi91@gmail.com");
        receipt.setCif("9100741778");
        receipt.setTglLahir("13061994");
        receipt.setCardPin("123456");
        receiptList.add(receipt);
        return receiptList;
    }
}
